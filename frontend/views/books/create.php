<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Books */

$this->title = Yii::t('app', 'Новая книга');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Books'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        

    ]);

?>
    
    <?php Pjax::begin(['id' => 'books']) ?>
    <?= DatePicker::widget([
        'name' => 'check_issue_date',
        'model' => $model,
        'attribute' => 'date_create',    

           'value' => date('d-M-Y', strtotime('+0 days')),
           'options' => ['placeholder' => 'Select issue date ...'],
           'pluginOptions' => [
               'format' => 'dd-M-yyyy',
               'todayHighlight' => true
           ],
    ]); ?>
    <?php Pjax::end() ?>



</div>
