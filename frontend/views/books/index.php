<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Книги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать книгу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'preview',
                'format' => 'html',
                'label' => 'Превью',
                 'value' => function ($data) {
                            return Html::img(Yii::getAlias('@web').'/images/'. $data['preview'],
                                ['width' => '70px']);
                 },
            ],
            'author_id',
            'date',
            'date_create',
            // 'date_update',
            ['class' => 'yii\grid\ActionColumn'],
        ],
]); ?>
<?php Pjax::end(); ?>
</div>


